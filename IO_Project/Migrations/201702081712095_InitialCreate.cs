namespace IO_Project.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Kartotekas",
                c => new
                    {
                        KartotekaId = c.Int(nullable: false, identity: true),
                        osoba_OsobaId = c.Int(),
                    })
                .PrimaryKey(t => t.KartotekaId)
                .ForeignKey("dbo.Osobas", t => t.osoba_OsobaId)
                .Index(t => t.osoba_OsobaId);
            
            CreateTable(
                "dbo.Mandats",
                c => new
                    {
                        MandatId = c.Int(nullable: false, identity: true),
                        kwota = c.Single(nullable: false),
                        artykulWykroczenia = c.String(),
                        punktyKarne = c.Int(nullable: false),
                        Kartoteka_KartotekaId = c.Int(),
                    })
                .PrimaryKey(t => t.MandatId)
                .ForeignKey("dbo.Kartotekas", t => t.Kartoteka_KartotekaId)
                .Index(t => t.Kartoteka_KartotekaId);
            
            CreateTable(
                "dbo.Osobas",
                c => new
                    {
                        OsobaId = c.Int(nullable: false, identity: true),
                        imie = c.String(),
                        nazwisko = c.String(),
                        data_urodzenia = c.String(),
                        PESEL = c.String(),
                    })
                .PrimaryKey(t => t.OsobaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Kartotekas", "osoba_OsobaId", "dbo.Osobas");
            DropForeignKey("dbo.Mandats", "Kartoteka_KartotekaId", "dbo.Kartotekas");
            DropIndex("dbo.Mandats", new[] { "Kartoteka_KartotekaId" });
            DropIndex("dbo.Kartotekas", new[] { "osoba_OsobaId" });
            DropTable("dbo.Osobas");
            DropTable("dbo.Mandats");
            DropTable("dbo.Kartotekas");
        }
    }
}
