﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using IO_Project.model;
namespace IO_Project
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            RoutedEventArgs e = new RoutedEventArgs();
            refreshClick(this.osobyCombo, e);
            
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            using (var db = new DBKartoteka())
            {
                var db_set = db.Ososby.Where(b => b.PESEL == this.textBox3.Text).ToList();
                if (db_set.Count == 0)
                {
                    Osoba a = new Osoba();
                    a.imie = this.textBox.Text;
                    a.nazwisko = this.textBox1.Text;
                    a.data_urodzenia = this.textBox2.Text;
                    a.PESEL = this.textBox3.Text;
                    db.Ososby.Add(a);
                    db.SaveChanges();
                }
             
            }
        
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            using (var db = new DBKartoteka())
            {
                Mandat a = new Mandat();
                a.artykulWykroczenia = this.artykul_wykorczenia.Text;
                double temp = 0;
                Double.TryParse(this.kwota_mandatu.Text, out temp);
                a.kwota = (float)temp;
                a.punktyKarne = Int32.Parse(this.punkty_karne.Text);
                a.OsobaId = this.osobyCombo.SelectedIndex;
                db.mandaty.Add(a);
                db.SaveChanges();
            }
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void refreshClick(object sender, RoutedEventArgs e)
        {
            using (var db = new DBKartoteka())
            {
                
                this.osobyCombo.ItemsSource = db.Ososby.ToList();
                this.osobyCombo.DisplayMemberPath = "imie_nazwisko";
                this.osobyCombo.SelectedValuePath = "OsobaId";
                this.osobyCombo.SelectedIndex = 0;
            }
        
        }
    }
}
